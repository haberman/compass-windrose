#!/usr/bin/env python3
# -*- coding:utf-8 -*-


import json
import os

if __name__ == '__main__':
    path = './'
    files = os.listdir(path)

    for f in files:
        f = os.path.join(path, f)
        if os.path.isdir(f) or os.path.basename(f).endswith('.py') or os.path.basename(f).startswith('world_'):
            continue

        with open(f, 'r') as json_file:
            print(f'loading "{f}"')

            rev_dict = {}
            json_data = json.load(json_file)['data']
            for feminicide in json_data:
                for key, value in feminicide.items():
                    if key != 'who':
                        continue
                    try:
                        rev_dict.setdefault(value['name'], set()).add('name')
                    except KeyError:
                        continue

            result = [key for key, values in rev_dict.items() if len(values) > 1]

            if len(result) > 0:
                print(f'duplicate values: {result}')
            else:
                print('no duplicates')
