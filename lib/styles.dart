import 'package:flutter/material.dart';

const needleNorthColor = Colors.red;
const needleSouthColor = Colors.white;

const degreeTextStyle = TextStyle(
  fontFamily: "Roboto",
  color: Colors.white,
  fontSize: 15,
);

const TextStyle cardinalTextStyle = TextStyle(
  fontFamily: 'RobotoCondensed',
  fontWeight: FontWeight.bold,
  color: Colors.black,
  // shadows: [ // fake border outline
  //   const Shadow(color: Colors.white, offset: Offset(0, -1)),
  //   const Shadow(color: Colors.white, offset: Offset(1, -1)),
  //   const Shadow(color: Colors.white, offset: Offset(1, 0)),
  //   const Shadow(color: Colors.white, offset: Offset(1, 1)),
  //   const Shadow(color: Colors.white, offset: Offset(0, 1)),
  //   const Shadow(color: Colors.white, offset: Offset(-1, 1)),
  //   const Shadow(color: Colors.white, offset: Offset(-1, 0)),
  //   const Shadow(color: Colors.white, offset: Offset(-1, -1))
  // ],
  fontSize: 19,
);

// TextStyle cardinalFGTextStyle = cardinalBGTextStyle.copyWith(
//   color: Colors.white,
//   fontSize: 24,
//   // foreground: Paint()
//   //   ..style = PaintingStyle.stroke
//   //   ..strokeWidth = 2
//   //   ..color = Colors.red,
// );

TextStyle distanceTextStyle = degreeTextStyle.copyWith(fontSize: 24);

const identityTextStyle = TextStyle(
  fontFamily: "Simonetta",
  fontWeight: FontWeight.w400,
  color: Colors.white,
  fontSize: 24,
);

TextStyle contextTextStyle = identityTextStyle.copyWith(fontSize: 19);
