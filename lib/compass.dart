import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:vector_math/vector_math_64.dart' show Vector2;

import 'bloc.dart';
import 'feminicide.dart';
import 'geo_utils.dart' show bearing, distance, degreeToCardinal;
import 'math_utils.dart' show PI_2, PI_4, deg2Rad;
import 'styles.dart' as styles;

class RulerOption {
  final num start, stop, declination, resolution;

  const RulerOption({
    this.start = 0,
    this.stop = 360,
    this.declination = 0,
    this.resolution = 1,
  }) : assert(start < stop);
}

class MarkOption {
  final Color color;
  final num length, width;
  final num step;
  final bool inner;

  const MarkOption({
    @required this.step,
    this.length = 10,
    this.width = 3,
    this.color = Colors.white,
    this.inner = true,
  }) : assert(step != null);
}

class DegreeOption {
  final TextStyle style;
  final num shift, step;

  const DegreeOption({
    @required this.style,
    this.shift = 10,
    this.step = 15,
  }) : assert(style != null);
}

class CardinalOption {
  final TextStyle style;
  final double step, shift;

  CardinalOption({
    @required this.style,
    this.step = 90,
    this.shift = 60,
  })  : assert(style != null),
        assert(step % 22.5 == 0);
}

class WindroseOption {
  final Color color;
  final num step, shift, length, angle;

  const WindroseOption({
    this.color = Colors.white,
    this.step = 22.5,
    this.shift = 60,
    this.length = 60,
    this.angle = pi * .1,
  }) : assert(step % 22.5 == 0);
}

class Compass extends StatefulWidget {
  final RulerOption rulerOption;
  final WindroseOption windroseOption;
  final CardinalOption cardinalOption;

  final List<MarkOption> markOptions;
  final List<DegreeOption> degreeOptions;

  final TextStyle distanceTextStyle;

  final double radius;
  final Vector2 center;
  final Feminicide feminicide;

  const Compass({
    @required this.rulerOption,
    @required this.markOptions,
    @required this.radius,
    this.windroseOption,
    this.cardinalOption,
    this.degreeOptions,
    this.distanceTextStyle,
    this.center,
    this.feminicide,
  })  : assert(rulerOption != null),
        assert(rulerOption != null),
        assert(radius != null);

  @override
  State<StatefulWidget> createState() => CompassState();
}

class CompassState extends State<Compass> with TickerProviderStateMixin {
  AnimationController _controller;
  double _bearing, _distance;

  void _startAnimation() {
    _bearing = BlocProvider.of<AppBloc>(context).lastBearing;
    _distance = BlocProvider.of<AppBloc>(context).lastDistance;

    if (widget.feminicide != null || widget.center != null) {
      final where = Vector2(widget.feminicide.where.lng, widget.feminicide.where.lat);

      final d = distance(widget.center, where);
      final b = bearing(widget.center, where);

      final ms = (3000 + Random().nextDouble() * 3000).toInt();
      _controller = AnimationController(duration: Duration(milliseconds: ms), vsync: this);

      final bearingAnim = CurvedAnimation(parent: _controller, curve: Interval(0, 1, curve: Curves.elasticOut));
      final bearingTween = Tween<double>(begin: _bearing, end: b).animate(bearingAnim);

      final distanceAnim = CurvedAnimation(parent: _controller, curve: Interval(0, .5, curve: Curves.easeOut));
      final distanceTween = Tween<double>(begin: _distance, end: d).animate(distanceAnim);

      bearingTween
        ..addListener(() {
          setState(() {
            _bearing = bearingTween.value;
          });
        })
        ..addStatusListener((status) {
          if (status == AnimationStatus.completed) {
            BlocProvider.of<AppBloc>(context).lastBearing = b;
          }
        });

      distanceTween
        ..addListener(() {
          setState(() {
            _distance = distanceTween.value;
          });
        })
        ..addStatusListener((status) {
          if (status == AnimationStatus.completed) {
            BlocProvider.of<AppBloc>(context).lastDistance = d;
          }
        });

      _controller.forward();
    }
  }

  @override
  void initState() {
    super.initState();
    _startAnimation();
  }

  @override
  void didUpdateWidget(Compass oldWidget) {
    super.didUpdateWidget(oldWidget);
    _controller.dispose();
    _startAnimation();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    final compassPainter = CompassPainter(
      radius: widget.radius ?? min(screenSize.width, screenSize.height) / 3,
      bearing: _bearing,
      distance: _distance,
      rulerOption: widget.rulerOption,
      windroseOption: widget.windroseOption,
      cardinalOption: widget.cardinalOption,
      markOptions: widget.markOptions,
      degreeOptions: widget.degreeOptions,
    );
    return CustomPaint(
      painter: compassPainter,
      isComplex: true,
    );
  }
}

class CompassPainter extends CustomPainter {
  final RulerOption rulerOption;
  final WindroseOption windroseOption;
  final CardinalOption cardinalOption;

  final List<MarkOption> markOptions;
  final List<Paint> markPaints;
  final List<DegreeOption> degreeOptions;

  final double radius, bearing, distance;

  TextPainter _textPainter;

  CompassPainter({
    this.radius,
    this.bearing,
    this.distance,
    this.rulerOption,
    this.windroseOption,
    this.cardinalOption,
    this.markOptions,
    this.degreeOptions,
  }) : markPaints = markOptions
            .map((mark) => Paint()
              ..color = mark.color
              ..style = PaintingStyle.stroke
              ..strokeWidth = mark.width)
            .toList() {
    _textPainter = TextPainter(textDirection: TextDirection.ltr);
  }

  void _paintAnchor(final Canvas canvas, final double radius, double yShift) {
    final anchorRadius = radius * .04;
    final anchorLength = radius * .12;
    final anchorPaint = Paint()
      ..color = Colors.white
      ..style = PaintingStyle.stroke
      ..strokeWidth = radius * .01;

    canvas.drawCircle(Offset.zero, anchorRadius, anchorPaint);
    canvas.drawLine(
      Offset(0, -anchorRadius),
      Offset(0, -yShift - anchorLength * .66),
      anchorPaint,
    );
    canvas.drawLine(
      Offset(0, anchorRadius),
      Offset(0, yShift + anchorLength * .66),
      anchorPaint,
    );
  }

  void _paintNeedle(final Canvas canvas, final double radius, int opacity) {
    const opening = PI_4;

    final width = radius * .09;
    final cutRadius = Radius.circular(width * .5);

    final y1 = sin(PI_2 + opening) * -width, halfY1 = y1 * .5;
    final l1x = cos(PI_2 - opening) * -width;
    final r1x = cos(PI_2 + opening) * -width;
    final start1 = Offset(-width * .25, halfY1);

    canvas.drawPath(
        Path()
          ..moveTo(start1.dx, start1.dy)
          ..arcToPoint(Offset(l1x, y1), radius: cutRadius, clockwise: false)
          ..lineTo(0, -radius)
          ..lineTo(r1x, y1)
          ..arcToPoint(Offset(width * .25, halfY1), radius: cutRadius, clockwise: false)
          ..arcToPoint(start1, radius: cutRadius, clockwise: false),
        Paint()
          ..color = Colors.red.withAlpha(opacity)
          ..style = PaintingStyle.fill);

    final y2 = sin(PI_2 + opening) * width, halfY2 = y2 * .5;
    final l2x = cos(PI_2 - opening) * width;
    final r2x = cos(PI_2 + opening) * width;
    final start2 = Offset(width * .25, halfY2);

    canvas.drawPath(
        Path()
          ..moveTo(start2.dx, start2.dy)
          ..arcToPoint(Offset(l2x, y2), radius: cutRadius, clockwise: false)
          ..lineTo(0, radius)
          ..lineTo(r2x, y2)
          ..arcToPoint(Offset(-width * .25, halfY2), radius: cutRadius, clockwise: false)
          ..arcToPoint(start2, radius: cutRadius, clockwise: false),
        Paint()
          ..color = Colors.white.withAlpha(opacity)
          ..style = PaintingStyle.fill);
  }

  void _paintSupport(final Canvas canvas, final double radius, double yShift, int opacity) {
    final plateWidth = radius * .02, plateLength = radius * .16, plateRadius = plateWidth * .66;
    final sliverWidth = radius * .004, sliverLength = radius * .4;

    final northPlate = RRect.fromRectAndCorners(
        Rect.fromPoints(
          Offset(-plateWidth, -yShift - plateLength),
          Offset(plateWidth, -yShift),
        ),
        bottomLeft: Radius.circular(plateRadius),
        bottomRight: Radius.circular(plateRadius));

    final sliver = Path()
      ..moveTo(-plateWidth, 0)
      ..lineTo(-sliverWidth, -plateRadius)
      ..lineTo(-sliverWidth, -plateRadius - sliverLength)
      ..lineTo(sliverWidth, -plateRadius - sliverLength)
      ..lineTo(sliverWidth, -plateRadius)
      ..lineTo(plateWidth, 0);

    canvas.drawPath(
        Path()
          ..addRRect(northPlate)
          ..addPath(sliver, Offset(0, -yShift - plateLength)),
        Paint()
          ..color = Colors.red.withAlpha(opacity)
          ..style = PaintingStyle.fill);

    final southPlate = RRect.fromRectAndCorners(
        Rect.fromPoints(
          Offset(-plateWidth, yShift + plateLength),
          Offset(plateWidth, yShift),
        ),
        topLeft: Radius.circular(plateRadius),
        topRight: Radius.circular(plateRadius));

    final transform = Matrix4.identity()..rotateX(pi);
    canvas.drawPath(
        Path()
          ..addRRect(southPlate)
          ..addPath(sliver.transform(transform.storage), Offset(0, yShift + plateLength)),
        Paint()
          ..color = Colors.white.withAlpha(opacity)
          ..style = PaintingStyle.fill);
  }

  void _paintMarks(final Canvas canvas, final double radius) {
    for (num i = rulerOption.start; i < rulerOption.stop; i += rulerOption.resolution) {
      final rad = deg2Rad(i);
      final cosRad = cos(rad), sinRad = sin(rad);
      for (var j = 0; j < markOptions.length; ++j) {
        final markOption = markOptions[j];
        if (i % markOption.step != 0) {
          continue;
        }

        final markLength = markOptions[j].inner ? -markOptions[j].length : markOptions[j].length;
        final markRadius = radius + markLength;
        canvas.drawLine(
          Offset(cosRad * radius, sinRad * radius),
          Offset(cosRad * markRadius, sinRad * markRadius),
          markPaints[j],
        );

        break;
      }
    }
  }

  void _paintDegrees(final Canvas canvas, final double radius) {
    if (degreeOptions == null || degreeOptions.length == 0) {
      return;
    }

    for (final degreeOption in degreeOptions) {
      final inc = degreeOption.step % 360;
      for (num i = rulerOption.start; i < rulerOption.stop; i += inc) {
        final rad = deg2Rad(i - 90);
        final dist = radius + degreeOption.shift;

        final textSpan = TextSpan(text: i.toString(), style: degreeOption.style);
        _textPainter.text = textSpan;
        _textPainter.layout();

        canvas.save();
        canvas.translate(cos(rad) * dist, sin(rad) * dist);
        canvas.rotate(rad + PI_2);

        _textPainter.paint(canvas, Offset(_textPainter.width * -.5, _textPainter.height * -.5));

        canvas.restore();
      }
    }
  }

  void _paintWindrose(final Canvas canvas, final double radius) {
    //
    //        B
    //       / \
    //      /   \
    // mC _/     \_ mA
    //    c\     /a
    //   /  \   /  \
    //  /    \ /    \
    // A------|------C
    //        mB
    //
    // we're given angle ABC and a, c length;
    // canvas is set on origin (0,0) B with the given rotation around c (virtual center of the windrose);
    // design reference is: https://www.solasmarine.co.uk/templates/images/slides/commercial-compass-slide.jpg.
    // i is degree; in order we check: i % 90   == 0 -> we draw iso triangle A-B-C;
    //                                 i % 45   == 0 -> we draw rhombus mC-B-mA-mB;
    //                                 i % 22.5 == 0 -> we draw iso triangle mC-B-mA.

    final s = windroseOption.shift, a = windroseOption.angle, l = windroseOption.length;
    final alt = l * cos(a * .5);

    final pA = Offset(cos(PI_2 - a) * l, sin(PI_2 - a) * l);
    final pC = Offset(cos(PI_2 + a) * l, sin(PI_2 + a) * l);

    // final mB = Offset.lerp(pA, pC, .5);
    // final mA = Offset(cos(PI_2 + a) * mL, sin(PI_2 + a) * mL);
    // final mC = Offset(cos(PI_2 - a) * mL, sin(PI_2 - a) * mL);

    final mB = Offset.lerp(pA, pC, .5);
    final mA = pC * .5;
    final mC = pA * .5;

    canvas.drawCircle(
        Offset.zero,
        2 + radius - s * .5 - alt * .5,
        Paint()
          ..color = Colors.white
          ..strokeWidth = 2
          ..style = PaintingStyle.stroke);

    canvas.drawCircle(
        Offset.zero,
        4 + radius - s * .5 - alt,
        Paint()
          ..color = Colors.white
          ..strokeWidth = 3
          ..style = PaintingStyle.stroke);

    for (num i = rulerOption.start; i < rulerOption.stop; i += windroseOption.step) {
      final rad = deg2Rad(i - 90);

      canvas.save();
      canvas.translate(cos(rad) * (radius - s + alt), sin(rad) * (radius - s + alt));
      canvas.rotate(rad + PI_2);

      final path = Path();
      if (i % 90 == 0) {
        path
          ..lineTo(pC.dx, pC.dy)
          ..arcToPoint(pA, radius: Radius.circular(radius - s));
      } else if (i % 45 == 0) {
        path..lineTo(mA.dx, mA.dy)..lineTo(mB.dx, mB.dy)..lineTo(mC.dx, mC.dy);
      } else if (i % 22.5 == 0) {
        path
          ..lineTo(mA.dx, mA.dy)
          ..arcToPoint(mC, radius: Radius.circular(radius - s + alt * .5));
      }

      path.close();
      canvas.drawPath(path, Paint()..color = windroseOption.color);

      canvas.restore();
    }
  }

  void _paintCardinal(final Canvas canvas, final double radius) {
    for (num i = rulerOption.start; i < rulerOption.stop; i += cardinalOption.step) {
      final rad = deg2Rad(i - 90);

      final dx = cos(rad) * (radius - cardinalOption.shift);
      final dy = sin(rad) * (radius - cardinalOption.shift);

      final txt = degreeToCardinal(i);

      canvas.save();
      canvas.translate(dx, dy);
      canvas.rotate(rad + PI_2);

      final bgTextSpan = TextSpan(text: txt, style: cardinalOption.style);
      _textPainter.text = bgTextSpan;
      _textPainter.layout();
      _textPainter.paint(canvas, Offset(_textPainter.width * -.5, _textPainter.height * -.5));

      canvas.restore();
    }
  }

  void _paintDistance(final Canvas canvas, final double radius) {
    String d;
    if (distance < 1000) {
      d = '${distance.toInt()} m';
    } else {
      d = (distance * .001).toStringAsFixed(1) + ' km';
    }

    final textSpan = TextSpan(text: d, style: styles.distanceTextStyle);
    _textPainter.text = textSpan;
    _textPainter.layout();

    _textPainter.paint(canvas, Offset(-_textPainter.width * .5, radius * .5));
  }

  @override
  void paint(Canvas canvas, Size size) {
    final s = radius * .075;
    final c = size.center(Offset(0, 0));
    final r = radius - 50;

    canvas.drawCircle(
        c,
        r,
        Paint()
          ..color = Colors.white
          ..style = PaintingStyle.stroke
          ..strokeWidth = 3);

    canvas.save();
    canvas.translate(c.dx, c.dy);

    // _paintMarks(canvas, r);
    // _paintDegrees(canvas, r);

    if (windroseOption != null) {
      _paintWindrose(canvas, radius);
    }
    if (cardinalOption != null) {
      _paintCardinal(canvas, radius);
    }

    _paintDistance(canvas, radius);

    canvas.rotate(rulerOption.declination);
    _paintMarks(canvas, r);
    _paintDegrees(canvas, r);

    canvas.rotate(bearing);

    _paintNeedle(canvas, radius, 100);
    _paintAnchor(canvas, radius, s);
    _paintSupport(canvas, radius, s, 170);

    canvas.restore();
  }

  @override
  bool shouldRepaint(CompassPainter oldDelegate) => oldDelegate.bearing != bearing;
}
