import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

import 'feminicide.dart';

abstract class AppEvent extends Equatable {
  const AppEvent();

  @override
  List<Object> get props;
}

class Load extends AppEvent {
  final BuildContext context;
  const Load(this.context);

  @override
  List<Object> get props => [context];
}

class Start extends AppEvent {
  final Duration duration;
  const Start(this.duration);

  @override
  List<Object> get props => [duration];
}

class Dispose extends AppEvent {
  final Duration after;
  final Feminicide feminicide;

  const Dispose(this.feminicide, this.after);

  @override
  List<Object> get props => [after, feminicide];
}

abstract class AppState extends Equatable {
  const AppState();

  @override
  List<Object> get props;
}

class AppIdle extends AppState {
  @override
  List<Object> get props => [];
}

class AppBroken extends AppState {
  final String why;
  const AppBroken(this.why);

  @override
  List<Object> get props => [why];
}

class AppLoaded extends AppState {
  final List<Map<String, dynamic>> data;
  const AppLoaded(this.data);

  @override
  List<Object> get props => data;
}

abstract class AppRunning extends AppState {
  final Feminicide feminicide;
  const AppRunning(this.feminicide);

  @override
  List<Object> get props => [feminicide];
}

class AppMagnetized extends AppRunning {
  AppMagnetized(Feminicide feminicide) : super(feminicide);
}

class AppDemagnetized extends AppRunning {
  AppDemagnetized(Feminicide feminicide) : super(feminicide);
}

class AppDemagnetizing extends AppRunning {
  AppDemagnetizing(Feminicide feminicide) : super(feminicide);
}

class AppBloc extends Bloc<AppEvent, AppState> {
  List<Map<String, dynamic>> data;
  double lastBearing, lastDistance;
  WebSocketChannel channel;

  AppBloc()
      : data = [],
        lastBearing = 0,
        lastDistance = 0;

  @override
  AppState get initialState => AppIdle();

  @override
  Stream<AppState> mapEventToState(AppEvent event) async* {
    if (event is Load) {
      final assetBundle = DefaultAssetBundle.of(event.context);
      channel = WebSocketChannel.connect(Uri(scheme: 'ws', host: 'ext.penumbra.me', port: 9004));

      try {
        for (final id in ['be_2018', 'be_2019', 'be_2020', 'fr_2020', 'fr_2019', 'world_stats']) {
          final jsonString = await assetBundle.loadString('json/$id.json');
          final jsonObject = await json.decode(jsonString);

          for (final feminicide in jsonObject['data']) {
            data.add({'used': false, 'feminicide': Feminicide.fromJson(feminicide)});
          }
        }

        yield AppLoaded(data);
      } catch (e) {
        yield AppBroken('Failed while loading data:: $e');
      }
    } else if (event is Start) {
      Iterable unused = data.where((item) => item['used'] == false);

      if (unused.length == 0) {
        data.forEach((item) {
          item['used'] = false;
        });

        unused = data;
      }

      final index = Random().nextInt(unused.length);
      final feminicide = unused.elementAt(index)['feminicide'];

      yield AppMagnetized(feminicide);

      channel.sink.add(json.encode({'app': 'compass', 'data': feminicide.toJson()}));
      await Future.delayed(event.duration);

      yield AppDemagnetizing(feminicide);
    } else if (event is Dispose) {
      final index = data.indexWhere((item) => item['feminicide'] == event.feminicide);
      data[index]['used'] = true;

      yield AppDemagnetized(event.feminicide);
    }
  }
}
