class Who {
  final int id, age;
  final String name;

  Who(this.id, this.name, this.age);
  factory Who.fromJson(Map<String, dynamic> json) => Who(
        json['id'] ?? null,
        json['name'] ?? null,
        json['age'] ?? null,
      );

  Map<String, dynamic> toJson() => {'id': id, 'name': name, 'age': age};
}

class Where {
  final double lng, lat;
  final String country, department, locality, route;

  Where(this.lng, this.lat, this.country, this.department, this.locality, [this.route]);
  factory Where.fromJson(Map<String, dynamic> json) => Where(
        json['lng'],
        json['lat'],
        json['country'],
        json['department'] ?? null,
        json['locality'] ?? null,
        json['route'] ?? null,
      );

  Map<String, dynamic> toJson() => {
        'lng': lng,
        'lat': lat,
        'country': country,
        'department': department,
        'locality': locality,
      };
}

class How {
  final String src;
  final String excerpt;
  final List<String> lines;

  How(this.src, this.excerpt, this.lines);
  factory How.fromJson(Map<String, dynamic> json) => How(
        json['src'],
        json['excerpt'],
        json.containsKey('lines') ? List<String>.from(json['lines']) : null,
      );

  Map<String, dynamic> toJson() => {'src': src, 'excerpt': excerpt};
}

class Feminicide {
  final Who who;
  final Where where;
  final How how;
  final DateTime when;
  final List<String> links;

  Feminicide(this.who, this.where, this.how, this.when, this.links);
  factory Feminicide.fromJson(Map<String, dynamic> json) => Feminicide(
        json.containsKey('who') ? Who.fromJson(json['who']) : null,
        Where.fromJson(json['where']),
        How.fromJson(json['how']),
        DateTime.parse(json['when']),
        json.containsKey('links') ? List<String>.from(json['links']) : null,
      );

  Map<String, dynamic> toJson() => {
        'who': who?.toJson() ?? null,
        'where': where.toJson(),
        'when': when.toUtc().millisecondsSinceEpoch,
        'how': how.toJson(),
      };
}
