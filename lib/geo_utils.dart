import 'dart:math';

import 'package:compass/math_utils.dart' show deg2Rad, TAU;
import 'package:vector_math/vector_math_64.dart' show Vector2;

/// Earth Radius used with the Harvesine formula and approximates using a spherical (non-ellipsoid) Earth.
const earthRadius = 6371008.8;

/// Takes two `Vector2` and finds the geographic bearing between them,
/// i.e. the angle measured in radians from the north line.
/// see: https://github.com/Turfjs/turf/blob/master/packages/turf-bearing
double bearing(Vector2 start, Vector2 end) {
  final lng1 = deg2Rad(start[0]), lng2 = deg2Rad(end[0]);
  final lat1 = deg2Rad(start[1]), lat2 = deg2Rad(end[1]);

  final a = sin(lng2 - lng1) * cos(lat2);
  final b = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(lng2 - lng1);

  return atan2(a, b) % TAU;
}

/// Takes one or more [points] and calculates the centroid using the mean of all vertices.
/// This lessens the effect of small islands and artifacts when calculating the centroid of a set of polygons.
/// see: https://github.com/Turfjs/turf/blob/master/packages/turf-centroid
Vector2 centroid(List<Vector2> points) {
  double xSum = 0, ySum = 0;

  points.forEach((point) {
    xSum += point[0];
    ySum += point[1];
  });

  return Vector2(xSum / points.length, ySum / points.length);
}

/// Calculates the distance between two `Vector2` in meters.
/// This uses the [Haversine formula](http://en.wikipedia.org/wiki/Haversine_formula) to account for global curvature.
/// see: https://github.com/Turfjs/turf/blob/master/packages/turf-distance
double distance(Vector2 from, Vector2 to) {
  final dLat = deg2Rad(to[1] - from[1]);
  final dLon = deg2Rad(to[0] - from[0]);
  final lat1 = deg2Rad(from[1]);
  final lat2 = deg2Rad(to[1]);

  final a = pow(sin(dLat / 2), 2) + pow(sin(dLon / 2), 2) * cos(lat1) * cos(lat2);
  final b = 2 * atan2(sqrt(a), sqrt(1 - a));

  return b * earthRadius;
}

String degreeToCardinal(num degree) {
  const trigger = 22.5;

  if (degree == 0) {
    return 'N';
  } else if (degree == trigger) {
    return 'NNE';
  } else if (degree == 2 * trigger) {
    return 'NE';
  } else if (degree == 3 * trigger) {
    return 'ENE';
  } else if (degree == 4 * trigger) {
    return 'E';
  } else if (degree == 5 * trigger) {
    return 'ESE';
  } else if (degree == 6 * trigger) {
    return 'SE';
  } else if (degree == 7 * trigger) {
    return 'SSE';
  } else if (degree == 8 * trigger) {
    return 'S';
  } else if (degree == 9 * trigger) {
    return 'SSW';
  } else if (degree == 10 * trigger) {
    return 'SW';
  } else if (degree == 11 * trigger) {
    return 'WSW';
  } else if (degree == 12 * trigger) {
    return 'W';
  } else if (degree == 13 * trigger) {
    return 'WNW';
  } else if (degree == 14 * trigger) {
    return 'NW';
  } else if (degree == 15 * trigger) {
    return 'NNW';
  } else {
    return '';
  }
}
