import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:vector_math/vector_math_64.dart' show Vector2;

import 'bloc.dart';
import 'compass.dart';
import 'feminicide.dart';
import 'math_utils.dart' show deg2Rad;
import 'styles.dart' as styles;

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setEnabledSystemUIOverlays([]);

  // initializeDateFormatting('fr_FR', null).then((_) => runApp(App()));
  runApp(App());
}

class App extends StatelessWidget {
  const App({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final degreeOption = DegreeOption(
      style: styles.degreeTextStyle,
      step: 10,
      shift: -30,
    );

    // final paris = Vector2(2.3514616, 48.8566969);
    // final mil = Vector2(4.185849, 50.479084);
    final center = Vector2(4.185849, 50.479084);

    return MaterialApp(
      home: Material(
        type: MaterialType.transparency,
        child: Container(
          decoration: BoxDecoration(color: Colors.black),
          child: BlocProvider<AppBloc>(
            create: (BuildContext context) => AppBloc(),
            child: BlocBuilder<AppBloc, AppState>(builder: (context, state) {
              final deviceSize = MediaQuery.of(context).size;
              final compassRadius = max(deviceSize.height * .35, 300);

              // final compassSize = compassRadius * 2;
              // final remainingWidth = deviceSize.width - compassSize;
              // final flexSpace = deviceSize.width / 30;

              final rulerOption = RulerOption(resolution: 1, declination: deg2Rad(-163)); // deg2Rad(-163)
              final majMarkOption = MarkOption(step: 10, length: 20);
              final minMarkOption = MarkOption(step: 1, width: 1, length: 10);

              // final windroseOption = WindroseOption(
              //   color: Colors.white,
              //   step: 22.5,
              //   shift: compassRadius * .44,
              //   length: compassRadius * .22,
              //   angle: pi * .15,
              // );
              // final cardinalOption = CardinalOption(
              //   style: styles.cardinalTextStyle,
              //   shift: windroseOption.shift - styles.cardinalTextStyle.fontSize * 1.5,
              // );

              final bloc = BlocProvider.of<AppBloc>(context);
              Feminicide feminicide;

              if (state is AppIdle) {
                bloc.add(Load(context));
              } else if (state is AppRunning) {
                feminicide = state.feminicide;
              } else if (state is AppBroken) {
                return Center(
                  child: Text(
                    state.why,
                    style: TextStyle(color: Colors.redAccent, fontSize: 18),
                  ),
                );
              }

              if (state is AppLoaded || state is AppDemagnetized) {
                final duration = Duration(seconds: 13 + Random().nextInt(7));
                bloc.add(Start(duration));
              } else if (state is AppDemagnetizing) {
                bloc.add(Dispose(state.feminicide, Duration(seconds: 1)));
              }

              if (feminicide == null) {
                return Center(
                  child: SizedBox(
                    child: CircularProgressIndicator(),
                    width: 60,
                    height: 60,
                  ),
                );
              } else {
                return Center(
                  child: Compass(
                    center: center,
                    radius: compassRadius,
                    rulerOption: rulerOption,
                    markOptions: [majMarkOption, minMarkOption],
                    degreeOptions: [degreeOption],
                    distanceTextStyle: styles.distanceTextStyle,
                    feminicide: feminicide,
                  ),
                );

                // final dateDiff = DateTime.now().difference(feminicide.when);
                // final name = feminicide.who.name;
                // final when = "${feminicide.who.age} ans, il y a ${dateDiff.inDays} jours.";
                // final where = '${feminicide.where.locality} (${feminicide.where.department}).';

                // return Row(
                //   mainAxisAlignment: MainAxisAlignment.start,
                //   crossAxisAlignment: CrossAxisAlignment.center,
                //   children: <Widget>[
                //     Container(
                //       padding: EdgeInsets.only(
                //         left: compassRadius - flexSpace + remainingWidth * .5,
                //         right: compassRadius,
                //       ),
                //       child: Compass(
                //         radius: compassRadius,
                //         rulerOption: rulerOption,
                //         // windroseOption: windroseOption,
                //         cardinalOption: cardinalOption,
                //         markOptions: [majMarkOption, minMarkOption],
                //         degreeOptions: [degreeOption],
                //         distanceTextStyle: styles.distanceTextStyle,
                //         feminicide: feminicide,
                //         center: center,
                //       ),
                //     ),
                //     Container(
                //       child: AnimatedOpacity(
                //         duration: const Duration(seconds: 1),
                //         opacity: state is AppDemagnetizing ? 0 : 1,
                //         child: Column(
                //           mainAxisAlignment: MainAxisAlignment.center,
                //           crossAxisAlignment: CrossAxisAlignment.start,
                //           children: [
                //             Text(name, style: styles.identityTextStyle),
                //             Text(when, style: styles.identityTextStyle),
                //             Text(where, style: styles.contextTextStyle),
                //           ],
                //         ),
                //       ),
                //     ),
                //   ],
                // );
              }
            }),
          ),
        ),
      ),
    );
  }
}
