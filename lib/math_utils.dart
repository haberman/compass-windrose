import 'dart:math';

import 'package:vector_math/vector_math_64.dart' show radians, degrees;

const TAU = pi * 2;
const PI_2 = pi * .5;
const PI_4 = pi * .25;

double rad2Deg(double r) => degrees(r % TAU);
double deg2Rad(double d) => radians(d % 360);
