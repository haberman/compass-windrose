# compass

Code behind [Compass \[iter #1\]](http://art2.network/works/compass/windrose/#/), artwork presented during the [Digital Icons – Festival Transnumeriques #7](http://transcultures.be/2020/02/10/digital-icons-transnumeriques-7-la-louviere/).

## Extract
La question posée avec _Compass_ est de ce registre, de l'ordre d'une réduction maximale de la représentation cartographique d'un événement, un point de l'espace-temps (pour cette première itération du projet : le lieu d'un féminicide), un centre
d'observation (La Louvière) et une mesure (une direction) sont les termes de l’oscillation funeste d’un compas numérique, un cénotaphe éphémère proposant un lieu de recueillement ; le tout pointant sans trêve l’existence d’une disparition.
